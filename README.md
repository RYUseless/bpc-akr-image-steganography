# AKR -> Image Steganography
> origin of this code is in our private repo, but we need this repo for submiting project (it needs to be public *I think*).
# Content of this repo
- ``example_pictures/`` it is a folder with some encoded pictures and stuff for proof that the code itself works <br>
- ``steganography_code/`` folder that has the code itself in it
# Initial setup
#### Windows setup:
- firstly, cd into project file   
> in``steganography_code`` is the code(i think at least)
- then create a vtirtual enviroment with this command:
 ```shell
$ python -m venv vevn
```
 - now run venv
```shell
$ venv\Scripts\activate.bat
```
> use to correct ``\``, or you will suffer greatly
- now it should display ``venv`` before your current directory.
- And lastly install requered modules:
```shell
$ pip install dependencies
```
> if there are any issues with installing required modules, use this command:
> ```shell
> $ pip install -r Requirements.txt
> ```
* * *
 #### Linux setup:
 - Once again, like in windows, cd to project file (or open terminal in intellij)
 - create an enviroment:
```shell
$ python3 -m venv .venv
```
- activate venv:
```shell
$ source .venv/bin/activate
```
<!---
I hope at least :)
-->
- And lastly install dependencies:
> You should be in the ``.venv/``, if not, cd there
 ```shell
 $ pip install dependencies
 ```
> I found out, that ``tk`` package needs to be installed in the OS itself, so:<br>
> This depends on your linux distro, for example in arch based distros:
> ```shell
> $ Sudo pacman -S tk
> ```
> And if there are any issues with installing required modules, use this command: <br>
> ```shell
> $ pip install -r Requirements.txt
> ```

# How to use
- after you start the code in intellij (recomended) you should see gui pop up on your screen.  
- Now as the arrow guids you, click here to add a picture
![screenshot01](how_to_use_pictures/1.png)
-Now, you need to click on the "select file" button and find picture of your desires.
![screenshot01](how_to_use_pictures/2.png)
- After you find it, click on the picture(name). And if needed click on the "open" button
![screenshot01](how_to_use_pictures/3.png)
-  Now you need to click on "add image" button to see some magic!
![screenshot01](how_to_use_pictures/4.png)
- U surely want to hide some text, so click on the "encode" button!
![screenshot01](how_to_use_pictures/5.png)
- Now u need to click on "choose output name" <br>
![screenshot01](how_to_use_pictures/6.png)
- add name of the output image in ``*name*.png``<br>
![screenshot01](how_to_use_pictures/7.png)
- and now you can add a hidden message in the new picture(click on the "add hidden message and encode" button and than on the "confirm messange and encode"
>(dont mind the arrow, thats an error)
![screenshot01](how_to_use_pictures/8.png)
- It is finally time to add your secret message!
![screenshot01](how_to_use_pictures/9.png)
- and if u want to find out, what nice message is in the picture, just add the picture once again (the one with the text in it) and press "decode" button!
![screenshot01](how_to_use_pictures/10.png)
- help me, im brainded now
# Sources we used to get the idea about this project.
&emsp;&emsp;[[1] What is steganography](https://www.techtarget.com/searchsecurity/definition/steganography) <br>
&emsp;&emsp;[[2] How to cypher an text to an image](https://imagemagick.org/script/cipher.php) <br>
&emsp;&emsp;[[3] Just some motivational music](https://www.youtube.com/watch?v=dQw4w9WgXcQ) <br>


